maps = ARGF.to_a("\n\n", chomp: true)

seeds = maps.shift.then {|l| l[(l.index(?:)+1)..].split.map(&:to_i)}

maps.reduce(seeds) do |seeds, map|
  mapping = map.lines[1..].reduce(Hash.new {|h, k| k}) do |memo, line|
    dst, src, cnt = line.split.map(&:to_i)
    memo[src..(src+cnt)] = dst - src
    memo
  end
  seeds.map {|s| s + (mapping.each_pair.find(-> {0}) {|k, v| k.include? s}[1])}
end.then {|l| p l.min}
