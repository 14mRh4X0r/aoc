games = ARGF.each(chomp: true).to_a
games.each_with_index.reduce(Array.new(games.size, 1)) do |memo, (line, game)|
  wins = line[line.index(?:)..].split(?|)
                               .map {|a| a.split.map(&:to_i)}
                               .reduce(:&)
                               .size
  wins.times {|i| memo[game + i + 1] += memo[game]}
  memo
end.sum.then {|s| p s}
