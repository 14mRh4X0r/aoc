LIMITS = {
  "red" => 12,
  "green" => 13,
  "blue" => 14
}

ARGF.each(chomp: true).filter_map do |line|
  max = {"red" => 0, "green" => 0, "blue" => 0}
  game, sets = line.split(": ")
  game = game[/\d+/].to_i
  sets = sets.split("; ")
  sets.map! do |set|
    Hash[set.split(", ").map {|s| s.match(/(\d+) (\w+)/) { |m| [m[2], m[1].to_i]}}]
  end
  next if sets.any? {|s| LIMITS.any? {|k, v| s.fetch(k, 0) > v}}
  game
end.sum.then {|s| pp s}
