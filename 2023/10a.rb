# Order: e s w n
# piece => { in => out }
PIECES = {
  ?| => {s: :s, n: :n},
  ?- => {e: :e, w: :w},
  ?L => {s: :e, w: :n},
  ?J => {e: :n, s: :w},
  ?7 => {e: :s, n: :w},
  ?F => {w: :s, n: :e}
}

DIRECTIONS = {
  e: [0, 1],
  s: [1, 0],
  w: [0, -1],
  n: [-1, 0]
}


map = pp ARGF.each(chomp: true).map(&:chars)

start = map.each_with_index {|line, y| x = line.index(?S); break [y, x] unless x.nil?}

dir = DIRECTIONS.find {|dir, (dy, dx)| PIECES[map.dig(start[0] + dy, start[1] + dx)].include? dir}[0]
pp map, start, dir

