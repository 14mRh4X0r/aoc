NUMBERS = {
  "one" => "1",
  "two" => "2",
  "three" => "3",
  "four" => "4",
  "five" => "5",
  "six" => "6",
  "seven" => "7",
  "eight" => "8",
  "nine" => "9",
}

NUMBER_RE = /one|two|three|four|five|six|seven|eight|nine/

ARGF.map do |line|
  first = line.sub(NUMBER_RE, NUMBERS)
  last = line.sub(/(.*)(#{NUMBER_RE})/) { $1 + NUMBERS[$2] }
  (first[/\D*(\d).*/, 1] + last[/.*(\d)\D*/, 1]).to_i
end.sum.then {|s| pp s}
