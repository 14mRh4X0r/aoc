time, dist = ARGF.each(chomp: true).map {|l| l.split[1..].join.to_i}


# speed*speed - speed*time + dist = 0
# top: time/2
# top-intersection distance: .5 * sqrt(time * time - 4 * dist)
d = p Math.sqrt(time*time - 4 * dist) / 2
if time.even?
  2 * d.ceil - 1
else
  2 * d.round
end.then {|t| p t}
