# return a - b: a < b ? -1 : a == b ? 0 : 1
RANKS = Hash[%w[A K Q T 9 8 7 6 5 4 3 2 J].each_with_index.to_a]

def get_rank(hand)
  [get_type(hand), hand.chars.map(&RANKS)]
end

def get_type(hand)
  counts = hand.chars.group_by(&:itself).values.map(&:size)
  if counts.any? 5
    0
  elsif counts.any? 4
    1
  elsif counts.sort == [2, 3]
    2
  elsif counts.any? 3
    3
  elsif counts.count(2) == 2
    4
  elsif counts.any? 2
    5
  else
    6
  end
end

hands = ARGF.each(chomp: true).map {|line| line = line.split; line[1] = line[1].to_i; line}
hands.sort_by {|hand, bet| get_rank(hand)}
  .map(&:last)
  .reverse
  .each_with_index.map {|bet, i| bet * (i+1)}
  .sum
  .then {|s| p s}
