schema = ARGF.each(chomp: true).to_a
num_pos = schema.each_with_index.reduce({}) do |memo, (line, lineno)|
  offset = 0
  while idx = line.index(/\d/, offset)
    memo[[lineno, idx]] = line.match(/\d+/, idx)[0].to_i
    offset = line.index /\D/, idx
    break if offset.nil?
  end
  memo
end

num_pos.reduce(0) do |memo, ((y, x), i)|
  mag = Math.log10(i).to_i
  to_check = [y - 1, y, y + 1].flat_map do |y_|
    (x - 1..x + mag + 1).map do |x_|
      [y_, x_]
    end.keep_if do |(y_, x_)|
      y_ >= 0 && y_ < schema.size \
        && x_ >= 0 && x_ < schema[0].size
    end
  end
  memo += i if to_check.any? {|y_, x_| schema[y_][x_].match?(/[^\d.]/)}
  memo
end.then {|s| p s}
