maps = ARGF.to_a("\n\n", chomp: true)

seeds = maps.shift.then do |line|
  nums = line[(line.index(?:)+1)..].split.map(&:to_i)
  nums.each_slice(2).map {|start, length| Range.new(start, start + length, true)}
end

#pp seeds

maps.reduce(seeds) do |seeds, map|
  mapping = map.lines[1..].reduce([]) do |memo, line|
    dst, src, cnt = line.split.map(&:to_i)
    memo << [src...(src + cnt), dst - src]
  end.sort_by {|e| e.first.begin}
  #pp mapping: mapping
  seeds.flat_map do |seed|
    ss = mapping.reduce([]) do |memo, m|
      if seed.include? m.first.begin
        memo << (seed.begin...m.first.begin)
        seed = (m.first.begin...seed.end)
        #p include_begin: [memo, seed]
      end
      if seed.include? m.first.end
        memo << ((seed.begin + m.last)...(m.first.end + m.last))
        seed = (m.first.end...seed.end)
        #p include_end: [memo, seed]
      end
      if m.first.cover? seed
        memo << ((seed.begin + m.last)...(seed.end + m.last))
        #p cover: [memo, seed]
        break memo
      end
      memo
    end
    if ss.empty?
      #p empty: seed
      seed
    else
      ss
    end
  end#.tap {|ss| p flat_map: ss}
     .sort_by {|s| s.begin}
     .reduce([]) do |memo, seed|
       if !memo.empty? && memo[-1].end == seed.begin
         memo[-1] = memo[-1].begin...seed.end
         memo
       elsif seed.size == 0
         memo
       else
         memo << seed
       end
     end#.tap {|ss| p reduce: ss}
end.then {|l| p l.first.begin}
