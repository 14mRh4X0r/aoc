# return a - b: a < b ? -1 : a == b ? 0 : 1
RANKS = Hash[%w[A K Q T 9 8 7 6 5 4 3 2 J].each_with_index.to_a]

def get_rank(hand)
  [get_type(hand), hand.chars.map(&RANKS)]
end

def get_type(hand)
  chars = hand.chars
  js = chars.count(?J)
  return 0 if js == 5
  chars.delete(?J)
  counts = chars.group_by(&:itself).values.map(&:size).sort.reverse
  counts[0] += js
  if counts[0] >= 5
    0
  elsif counts[0] == 4
    1
  # AAT6J
  elsif counts[0..1] == [3, 2]
    2
  elsif counts[0] == 3
    3
  elsif counts[0..1] == [2, 2]
    4
  elsif counts[0] == 2
    5
  else
    6
  end
end

hands = ARGF.each(chomp: true).map {|line| line = line.split; line[1] = line[1].to_i; line}
hands.sort_by {|hand, bet| get_rank(hand)}
  .map(&:last)
  .reverse
  .each_with_index.map {|bet, i| bet * (i+1)}
  .sum
  .then {|s| p s}
