ARGF.map {|l| l.split.map(&:to_i)}.map do |series|
  diffs = [series]
  diffs << diffs.last.each_cons(2).map {|a, b| b - a} until diffs.last.all? 0
  diffs.map(&:last).reduce(:+)
end.sum.tap {|s| p s}
