ARGF.each(chomp: true).reduce(0) do |memo, line|
  wins = line[line.index(?:)..].split(?|)
                               .map {|a| a.split.map(&:to_i)}
                               .reduce(:&)
                               .size
  memo += 2**(wins - 1) if wins > 0
  memo
end.then {|n| pp n}
