ARGF.each(chomp: true).filter_map do |line|
  max = {"red" => 0, "green" => 0, "blue" => 0}
  game, sets = line.split(": ")
  game = game[/\d+/].to_i
  sets = sets.split("; ")
  sets.each do |set|
    h = Hash[set.split(", ").map {|s| s.match(/(\d+) (\w+)/) { |m| [m[2], m[1].to_i]}}]
    h.each {|k, v| max[k] = v if max[k] < v}
  end
  max.values.reduce(:*)
end.sum.then {|s| pp s}
