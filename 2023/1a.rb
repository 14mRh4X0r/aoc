ARGF.map do |line|
  [/\D*(\d).*/, /.*(\d)\D*/].map {|r| r.match(line)[1]}.join.to_i
end.sum.then {|s| pp s}
