turns = ARGF.readline(chomp: true).chars.cycle
ARGF.readline
map = Hash[ARGF.each(chomp: true).map {|l| l =~ /(\w+) = \((\w+), (\w+)\)/; [$1, [$2, $3]]}]


periods = map.keys.grep(/A$/).map do |pos|
  turns.each_with_index.inject(pos) do |pos, (turn, i)|
    break i if /Z$/ === pos
    map[pos][turn == ?R ? 1 : 0]
  end
end

p periods.reduce(:lcm)
