schema = ARGF.each(chomp: true).to_a
pos = schema.each_with_index.reduce({gear: [], num: {}}) do |memo, (line, lineno)|
  offset = 0
  while idx = line.index(/(\d|\*)/, offset)
    line.match(/\G\d+/, idx) do |m|
      num = m[0].to_i
      Math.log10(num).to_i.succ.times do |i|
        memo[:num][[lineno, idx + i]] = [num, [lineno, idx]]
      end
      offset = line.index /\D/, idx
    end
    if line[idx] == "*"
      memo[:gear] << [lineno, idx]
      offset = idx + 1
    end
    break if offset.nil?
  end
  memo
end

pos[:gear].reduce(0) do |memo, (y, x)|
  nums = [y - 1, y, y + 1].flat_map do |y_|
    [x - 1, x, x + 1].map do |x_|
      [y_, x_]
    end.keep_if do |(y_, x_)|
      y_ >= 0 && y_ < schema.size \
        && x_ >= 0 && x_ < schema[0].size
    end
  end.filter_map {|(y_, x_)| pos[:num][[y_, x_]]}
     .uniq {|e| e[1]}
     .map {|e| e[0]}

  memo += nums.reduce(:*) if nums.size == 2
  memo
end.then {|s| p s}
