#!/usr/bin/ruby

points, folds = ARGF.read.split "\n\n"
points = points.lines(chomp: true).map do |line|
  line.split(',').map(&:to_i)
end

folds = folds.lines(chomp: true).map do |line|
  axis, line = line[11..].split('=')
  [axis.to_sym, line.to_i]
end

folds[0,1].each do |(axis, line)|
  points.map! do |(x, y)|
    if axis == :x and x > line
      [2 * line - x, y]
    elsif axis == :y and y > line
      [x, 2 * line - y]
    else
      [x, y]
    end
  end
end
p points.uniq.size
