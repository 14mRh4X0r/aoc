#!/usr/bin/ruby
require 'bundler/inline'
gemfile do
  source 'https://rubygems.org'

  gem 'activesupport', require: false
end
require 'active_support/core_ext/object/deep_dup'

module DigSet
  refine Array do
    def dig_set(value, *path)
      last = path.pop
      dig(*path)[last] = value
    end
  end
end

module Magnitude
  refine Array do
    def magnitude
      raise ValueError if size != 2
      3 * self[0].magnitude + 2 * self[1].magnitude
    end
  end
end


class SNum
  using Magnitude

  def initialize(left, right)
    @arr = [parse(left), parse(right)].freeze
  end

  def +(other)
    SNum.new *SNum.reduce([self.arr, other.arr].deep_dup)
  end

  def magnitude
    @arr.magnitude
  end

  def inspect
    "SNum#{arr.inspect}"
  end

  alias :to_s :inspect

  def pretty_print(pp)
    pp.text "SNum"
    pp.pp arr
  end

  protected

  attr_accessor :arr

  def parse(side)
    case side
    when Integer
      side
    when SNum
      side.arr
    when Array
      raise ArgumentError, "Invalid snum: #{side.inspect}" if side.size != 2
      side.map { |e| parse(e) }
    else
      raise ArgumentError, "Invalid snum: #{side.inspect}"
    end
  end

  class << self
    using DigSet

    alias :[] :new

    def explode(arr, path = [])
      item = arr.dig(*path) unless path.empty?
      return arr if Integer === item

      if path.size == 4
        arr.dig_set(0, *path)

        # Add to left number
        unless path == [0] * 4
          leaf_path = path[0,path.rindex(1)] + [0]
          # Rightmost leaf node
          while arr.dig(*leaf_path).is_a? Array
            leaf_path << 1
          end
          arr.dig_set(arr.dig(*leaf_path) + item[0], *leaf_path)
        end

        # Add to right number
        unless path == [1] * 4
          leaf_path = path[0,path.rindex(0)] + [1]
          # Leftmost leaf node
          while arr.dig(*leaf_path).is_a? Array
            leaf_path << 0
          end
          arr.dig_set(arr.dig(*leaf_path) + item[1], *leaf_path)
        end

        # Reiterate
        reduce(arr)
      else
        expl_arr = explode(arr, path + [0])
        return reduce(arr) unless expl_arr == arr
        expl_arr = explode(arr, path + [1])
        return reduce(arr) unless expl_arr == arr
        arr
      end
    end

    def split(arr, path = [])
      item = arr.dig(*path) unless path.empty?

      if Integer === item
        return arr if item < 10

        arr.dig_set([item / 2, item / 2 + (item & 1)], *path)

        # Reiterate
        reduce(arr)
      else
        spl_arr = split(arr, path + [0])
        return reduce(arr) unless spl_arr == arr
        spl_arr = split(arr, path + [1])
        return reduce(arr) unless spl_arr == arr
        arr
      end
    end

    def reduce(arr, path = [])
      split(explode(arr))
    end
  end
end

snums = ARGF.map { |line| SNum.new(*eval(line)) }
max_sum = snums.permutation(2)
  .map { |(a, b)| a + b }
  .max_by(&:magnitude)
pp max_sum
p  max_sum.magnitude
