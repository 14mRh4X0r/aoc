#!/usr/bin/ruby
require 'set'
require 'stackprof'

map = ARGF.readlines.map(&:chomp).map {|line| line.each_char.map(&:to_i).to_a}

x_size = map[0].size
y_size = map.size

unvisited = Set[*x_size.times.to_a.product(y_size.times.to_a)]
dist = Array.new(x_size) { Array.new(y_size, Float::INFINITY) }
prev = Array.new(x_size) { Array.new(y_size) }

dist[0][0] = 0

StackProf.run(mode: :cpu, out: '15-1.dump', raw: true) do
  until unvisited.empty?
    x, y = unvisited.map {|(x, y)| [dist[y][x], [x, y]]}.min[1]
    unvisited.delete [x, y]
    break if [x, y] == [x_size - 1, y_size - 1]

    # north, east, south, west
    (Set[[x, y - 1], [x + 1, y], [x, y + 1], [x - 1, y]] & unvisited).each do |xp, yp|
      alt = dist[y][x] + map[yp][xp]
      if alt < dist[yp][xp]
        dist[yp][xp] = alt
        prev[yp][xp] = [x, y]
      end
    end
  end
end

p dist[-1][-1]
