#!/usr/bin/ruby

ones_count = nil

ARGF.each do |line|
  line.chomp!
  ones_count ||= [0] * line.length
  ones_count.collect!.with_index do |el, i|
    el + line[i].to_i
  end
end

puts ones_count.inspect, ARGF.lineno

gamma = ones_count.map {|e| e < (ARGF.lineno / 2) ? '1' : '0'}.join('').to_i 2
epsilon = ones_count.map {|e| e > (ARGF.lineno / 2) ? '1' : '0'}.join('').to_i 2

puts gamma * epsilon
