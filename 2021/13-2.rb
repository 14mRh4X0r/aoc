#!/usr/bin/ruby

points, folds = ARGF.read.split "\n\n"
points = points.lines(chomp: true).map do |line|
  line.split(',').map(&:to_i)
end

folds = folds.lines(chomp: true).map do |line|
  axis, line = line[11..].split('=')
  [axis.to_sym, line.to_i]
end

folds.each do |(axis, line)|
  points.map! do |(x, y)|
    if axis == :x and x > line
      [2 * line - x, y]
    elsif axis == :y and y > line
      [x, 2 * line - y]
    else
      [x, y]
    end
  end
end

points.uniq!

x_max = points.map {|p| p[0]}.max
y_max = points.map {|p| p[1]}.max

y_max.succ.times.map do |y|
  x_max.succ.times.map do |x|
    points.include?([x, y]) ? "█" : " "
  end.join
end.map {|l| puts l}
