#!/usr/bin/ruby

scanner_data = ARGF.read.split("\n\n").map do |data|
  data.lines(chomp: true).drop(1).map {|coord| coord.split(',').map(&:to_i)}
end

pp scanner_data
