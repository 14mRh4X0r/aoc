#!/usr/bin/ruby

# 3 bits version
# 3 bits type ID

# Type ID 4:
# groups of 5 bytes, MSB 0 indicates end, lower 4 bytes are value

# Type ID != 4:
# 1 bit: length in bytes (0) or packets (1)

# Byte length:
# 15 bits length + sub-packets

# Packet length:
# 11 bits number of sub-packets + sub-packets

data = [ARGF.gets(chomp: true)].pack('H*').unpack1('B*').chars

# Parse the given packet
# @param packet [Array<String>] The packet, consisting of an array of String bytes
# @return [(Integer, Array<String>)] Version and remaining bytes, respectively
def parse(packet)
  version = packet.shift(3).join.to_i(2)

  # Type
  case packet.shift(3).join.to_i(2)
  when 4 # Literal value
    packet.shift(5) while packet[0] == "1"
    packet.shift(5)
  else # Operator
    # Length type
    if packet.shift == "0" # Length in bytes
      subpackets = packet.shift(packet.shift(15).join.to_i(2))
      until subpackets.empty?
        subver, subpackets = parse(subpackets)
        version += subver
      end
    else # Length in packets
      packet_count = packet.shift(11).join.to_i(2)
      packet_count.times do
        subver, packet = parse(packet)
        version += subver
      end
    end
  end

  [version, packet]
end

p parse(data)[0]
