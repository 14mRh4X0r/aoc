#!/usr/bin/ruby
require 'set'
require 'pp'

DIGIT_SEGMENTS = [
  %w[a b c e f g],
  %w[c f],
  %w[a c d e g],
  %w[a c d f g],
  %w[b c d f],
  %w[a b d f g],
  %w[a b d e f g],
  %w[a c f],
  %w[a b c d e f g],
  %w[a b c d f g]
].map(&:to_set)

def by_size(n)
  proc {|e| e.size == n}
end

nums = ARGF.map do |line|
  digit_segments = Array.new(10)
  patterns, numbers = line.split(' | ').map do |part|
    part.split.map {|p| p.chars.to_set}
  end

  digit_segments[1] = patterns.find &by_size(2)
  digit_segments[7] = patterns.find &by_size(3)
  digit_segments[4] = patterns.find &by_size(4)
  digit_segments[8] = patterns.find &by_size(7)

  bottomless_9 = digit_segments[4] | digit_segments[7]
  digit_segments[9] = (patterns - digit_segments).find {|p| (p ^ bottomless_9).size == 1}
  digit_segments[0] = (patterns - digit_segments).find {|p| p.size == 6 && p > digit_segments[1]}
  digit_segments[6] = (patterns - digit_segments).find &by_size(6)
  digit_segments[5] = digit_segments[6] & digit_segments[9]

  digit_segments[3] = (patterns - digit_segments).find {|p| p > digit_segments[1]}
  digit_segments[2] = (patterns - digit_segments).first

  numbers.map {|n| digit_segments.find_index(n)}.join.to_i
end

p nums.sum
