#!/usr/bin/perl -n

BEGIN {
    $increases = 0;
    $prev = -1;
}

chomp;
$num = +$_;
unless ($prev == -1) {
    if ($num > $prev) {
        $increases += 1;
    }
}
$prev = $num;

END {
    printf "%d\n", $increases;
}
