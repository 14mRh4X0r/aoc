#!/usr/bin/ruby

# Example input: "target area: x=20..30, y=-10..-5"

xrange, yrange = ARGF.gets(chomp: true)[13..].split(', ').map {|e| eval e}

# x[n] = x[n-1] + vx[n-1]
# x[n] = x[0] + n * (vx[0] - (n - 1)) / 2
# vx[n] = vx[n-1] - (vx[n-1] <=> 0)
# n(vx[n] == 0) = vx[0] - 1
# max[x[n]] = x[vx[0]] = vx > 0
#   ? x[0] + vx[0] * (vx[0] + 1) / 2
#   : x[0] - vx[0] * (vx[0] - 1) / 2

# y[n] = y[n-1] + vy[n-1]
# vy[n] = vy[n-1] - 1
# vy[n] = vy[0] - n
# max[y[n]] = vy < 0
#   ? vy
#   : vy[0] * (vy[0] + 1) / 2
#
# y reaches yt => yt = ymax - n * (n + 1) / 2 has positive integer n
# => n**2/2 + n / 2 + (yt - ymax) = 0
# => -1 + sqrt(1 - 4 * (yt - ymax)) / 2 = n
# => ymax > yt

# ---

# yrange always < 0

valid_vy = yrange.flat_map do |y|
  (y...-y).select do |vy|
    py = 0
    while py > y
      py += vy
      vy -= 1
    end
    py == y
  end
end.uniq

valid_vx = xrange.flat_map do |x|
  (0..x).select do |vx|
    px = 0
    while px < x && vx > 0
      px += vx
      vx -= 1
    end
    px == x
  end
end.uniq

valid_v = valid_vx.product(valid_vy).select do |(vx, vy)|
  px, py = 0, 0
  while px <= xrange.max && py >= yrange.min && !(xrange.include?(px) && yrange.include?(py))
    px += vx
    py += vy
    vx -= (vx <=> 0)
    vy -= 1
  end
  xrange.include?(px) && yrange.include?(py)
end

p valid_v.size
