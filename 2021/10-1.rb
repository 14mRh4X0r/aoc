#!/usr/bin/ruby

PAIRS = Hash['()[]{}<>'.chars.each_slice(2).to_a]
SCORE = {
  ')' => 3,
  ']' => 57,
  '}' => 1197,
  '>' => 25137
}

score = ARGF.map do |line|
  line.chomp.chars.reduce([]) do |stack, char|
    if PAIRS.has_key?(char)
      stack << char
    elsif char == PAIRS[stack[-1]]
      stack.pop
    else
      break [SCORE[char]]
    end
    stack
  end[0].to_i
end.sum

p score
