#!/usr/bin/ruby

arr = ARGF.read.split(',').map(&:to_i)
med = arr.sort[arr.length/2]
p arr.map {|e| (e - med).abs}.sum
