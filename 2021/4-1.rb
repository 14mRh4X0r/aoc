#!/usr/bin/ruby

draws = ARGF.readline.split(/,/).map(&:to_i)

boards = []

ARGF.each_slice(6) do |lines|
  lines.shift
  boards << lines.map {|line| line.split.map(&:to_i)}
end

win, drawn = draws.each_with_object([]) do |draw, drawn|
  drawn << draw
  win = boards.find do |board|
    board.any? do |row|
      (row - drawn).empty?
    end || board.transpose.any? do |col|
      (col - drawn).empty?
    end
  end
  break win, drawn unless win.nil?
end

p (win.flatten - drawn).sum * drawn[-1]
