#!/usr/bin/ruby
require 'pp'

draws = ARGF.readline.split(/,/).map(&:to_i)

boards = []

ARGF.each_slice(6) do |lines|
  lines.shift
  boards << lines.map {|line| line.split.map(&:to_i)}
end

last_win = nil
draws.each_with_object([]) do |draw, drawn|
  drawn << draw
  wins = boards.find_all do |board|
    board.any? { |row| (row - drawn).empty? } ||
      board.transpose.any? { |col| (col - drawn).empty? }
  end
  if wins.length == 1
    last_win = [wins[0], drawn]
  end
  boards -= wins
  break if boards.empty?
end

win, drawn = last_win

p (win.flatten - drawn).sum * drawn[-1]
