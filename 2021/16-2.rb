#!/usr/bin/ruby

# 3 bits version
# 3 bits type ID

# Type ID 4:
# groups of 5 bytes, MSB 0 indicates end, lower 4 bytes are value

# Type ID != 4:
# 1 bit: length in bytes (0) or packets (1)

# Byte length:
# 15 bits length + sub-packets

# Packet length:
# 11 bits number of sub-packets + sub-packets

data = [ARGF.gets(chomp: true)].pack('H*').unpack1('B*').chars

def calculate(type, n1, n2)
  return n2 if n1.nil?

  case type
  when 0 then n1 + n2
  when 1 then n1 * n2
  when 2 then [n1, n2].min
  when 3 then [n1, n2].max
  when 5 then n1 > n2 ? 1 : 0
  when 6 then n1 < n2 ? 1 : 0
  when 7 then n1 == n2 ? 1 : 0
  end
end

# Parse the given packet
# @param packet [Array<String>] The packet, consisting of an array of String bytes
# @return [(Integer, Array<String>] The result of evaluating the expression contained in the packet
def parse(packet)
  version = packet.shift(3).join.to_i(2)
  type = packet.shift(3).join.to_i(2)
  value = nil

  if type == 4 # Literal value
    value = []
    value += packet.shift(5)[1..] while packet[0] == "1"
    value += packet.shift(5)[1..]
    value = value.join.to_i(2)
  elsif packet.shift == "0" # Length in bytes
    subpackets = packet.shift(packet.shift(15).join.to_i(2))
    until subpackets.empty?
      subval, subpackets = parse(subpackets)
      value = calculate(type, value, subval)
    end
  else # Length in packets
    packet_count = packet.shift(11).join.to_i(2)
    packet_count.times do
      subval, packet = parse(packet)
      value = calculate(type, value, subval)
    end
  end

  [value, packet]
end

p parse(data)[0]
