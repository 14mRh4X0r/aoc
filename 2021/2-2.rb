#!/usr/bin/ruby

@depth = 0
@dist = 0
@aim = 0

def forward n
  @dist += n
  @depth += n * @aim
end

def up n
  @aim -= n
end

def down n
  @aim += n
end

STDIN.each &method(:eval)

puts @dist * @depth
