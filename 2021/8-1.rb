#!/usr/bin/ruby

puts(ARGF.map do |line|
  line.split(' | ').last.split.map(&:length).count {|n| [2, 3, 4, 7].include? n}
end.sum)
