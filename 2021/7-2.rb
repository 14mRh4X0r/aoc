#!/usr/bin/ruby

positions = ARGF.read.split(',').map(&:to_i)
best_try = 0
p (positions.max.times.reduce(Float::INFINITY) do |best, try|
  sum = positions.map do |pos|
    dist = (pos - try).abs
    dist * (dist + 1) / 2
  end.sum
  best_try = try if sum < best
  [sum, best].min
end)
p best_try
