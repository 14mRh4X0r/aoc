#!/usr/bin/ruby

lines = ARGF.readlines.map(&:chomp)
template, _ = lines.shift(2)

insn_map = Hash[lines.map {|line| line.split(' -> ')}]

pp template, insn_map

10.times do
  template = template.chars.reduce do |string, char|
    string << insn_map[string[-1] + char]
    string << char
  end
end

template.chars.tally.values.minmax.then {|min, max| p(max - min)}
