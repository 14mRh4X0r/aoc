#!/usr/bin/ruby
require 'pp'
require 'rgl/adjacency'
require 'rgl/dijkstra'

map = ARGF.readlines.map(&:chomp).map {|line| line.each_char.map(&:to_i).to_a}

map = 5.times.map do |y|
  map.map do |line|
    5.times.map do |x|
      line.map {|e| (e + x + y - 1) % 9 + 1}
    end.flatten(1)
  end
end.flatten(1)

x_size = map[0].size
y_size = map.size

weights = map.map.each_with_index do |row, y|
  row.map.each_with_index do |weight, x|
    [[x, y - 1], [x + 1, y], [x, y + 1], [x - 1, y]].filter_map do |xp, yp|
      next unless xp >= 0 && xp < x_size && yp >= 0 && yp < y_size
      [[[xp, yp], [x, y]], weight]
    end
  end
end.then {|e| Hash[e.flatten(2)]}

graph = RGL::DirectedAdjacencyGraph[*weights.keys.flatten(1)]
graph.dijkstra_shortest_path(weights, [0, 0], [x_size - 1, y_size - 1]).reduce(0) do |cost, (x, y)|
  cost + map[y][x]
end.then {|cost| p cost - map[0][0]}
