#!/usr/bin/ruby

PAIRS = Hash['()[]{}<>'.chars.each_slice(2).to_a]
SYN_SCORE = {
  ')' => 3,
  ']' => 57,
  '}' => 1197,
  '>' => 25137
}

AC_SCORE = {
  ')' => 1,
  ']' => 2,
  '}' => 3,
  '>' => 4
}

score = ARGF.map do |line|
  line.chomp.chars.reduce([]) do |stack, char|
    if PAIRS.has_key?(char)
      stack << char
    elsif char == PAIRS[stack[-1]]
      stack.pop
    else
      break []
    end
    stack
  end.map(&PAIRS).reverse.reduce(0) do |score, char|
    score * 5 + AC_SCORE[char]
  end
end.filter(&:positive?).sort.then {|arr| arr[arr.size / 2]}

p score
