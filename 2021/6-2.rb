#!/usr/bin/ruby

DAYS = 256

counts = ARGF.read
  .split(',')
  .map(&:to_i)
  .each_with_object([0] * 9) {|n, arr| arr[n] += 1}

DAYS.times do |_|
  counts.rotate!
  counts[6] += counts[8]
end

p counts.sum
