#!/usr/bin/ruby
require 'pp'
require 'gnuplot'

points = ARGF.map do |line|
  line.split(" -> ").map {|p| p.split(',').map(&:to_i)}
end

max_x = points.flatten(1).max {|p1, p2| p1[0] <=> p2[0]}[0]
max_y = points.flatten(1).max {|p1, p2| p1[1] <=> p2[1]}[1]

field = Array.new(max_y + 1) {|_i| Array.new(max_x + 1, 0)}

points.each do |p1, p2|
  x1, y1 = p1
  x2, y2 = p2
  if x1 == x2
    y1, y2 = [y1, y2].sort
    (y1..y2).each {|y| field[y][x1] += 1}
  elsif y1 == y2
    x1, x2 = [x1, x2].sort
    (x1..x2).each {|x| field[y1][x] += 1}
  else # diagonal
    p1, p2 = [p1, p2].sort
    p [p1, p2]
    r = p2[1] < p1[1] ? -1 : 1
    (p2[0] - p1[0]).next.times do |d|
      x = p1[0] + d
      y = p1[1] + d * r
      field[y][x] += 1
    end
  end
end

p field.map {|row| row.count {|cell| cell > 1}}.sum
