#!/usr/bin/ruby

prev_sum = -1
increases = 0
win = []

ARGF.each do |line|
  num = line.to_i
  win << num
  win.shift if win.length > 3
  sum = win.sum
  increases += 1 if prev_sum != -1 && sum > prev_sum
  prev_sum = sum if win.length == 3
end

puts increases
