#!/usr/bin/ruby

positions = ARGF.read.split(',').map(&:to_i)
avg = positions.sum.to_f / positions.length
tries = [avg.floor, avg.ceil]
best = tries.map do |try|
  positions.map do |pos|
    dist = (pos - try).abs
    dist * (dist + 1) / 2
  end.sum
end.min
p positions[positions.length/2]
p best
