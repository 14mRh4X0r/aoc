#!/usr/bin/ruby

height_map = ARGF.readlines.map {|line| line.chomp.chars.map(&:to_i)}

def get_minima hmap
  hmap.map do |y|
    xmin = []
    xmin << 0 if y[0] < y[1]
    xmin += y.each_cons(3).each_with_index.reduce([]) do |coll, arg|
      arr, i = arg
      coll << i + 1 if arr[0] > arr[1] && arr[2] > arr[1]
      coll
    end
    xmin << y.length - 1 if y[-1] < y[-2]
    xmin
  end.each_with_index.flat_map do |xmin, y|
    xmin.zip([y] * xmin.length)
  end
end

hmin = get_minima height_map
vmin = get_minima(height_map.transpose).map(&:reverse)
minima = hmin & vmin

p minima.map {|p| height_map[p[1]][p[0]] + 1}.sum
