#!/usr/bin/ruby

@depth = 0
@dist = 0

def forward n
  @dist += n
end

def up n
  @depth -= n
end

def down n
  @depth += n
end

ARGF.each &method(:eval)

puts @dist * @depth
