#!/usr/bin/ruby

fish = ARGF.read.split(',').map(&:to_i)

80.times do |_|
  fish.map!(&:pred)
  new = fish.reduce(0) {|new, f| f == -1 ? new.next : new}
  fish.map! {|f| f == -1 ? 6 : f}
  fish += [8] * new
end

p fish.length
