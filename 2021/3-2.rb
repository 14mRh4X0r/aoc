#!/usr/bin/ruby

@lines = ARGF.map(&:chomp).map(&:chars)
@digits = @lines[0].length

def filter(cmp)
  @digits.times.each_with_object(@lines.dup) do |i, my_lines|
    digit_sum = my_lines.transpose[i].map(&:to_i).sum
    my_lines.keep_if do |line|
      line[i] == (digit_sum.send(cmp, my_lines.length / 2.0) ? "1" : "0")
    end
    break my_lines[0] if my_lines.length == 1
  end.join.to_i 2
end

oxy = filter :>=
co2 = filter :<

puts oxy * co2
