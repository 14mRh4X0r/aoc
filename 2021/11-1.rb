#!/usr/bin/ruby
require 'set'

STEPS = 100

@dumbos = ARGF.readlines.map {|line| line.chomp.chars.map(&:to_i)}
@row_range = (0...@dumbos.size)
@col_range = (0...@dumbos[0].size)

def try_flash(flashed, x, y)
  if @dumbos[y][x] > 9 and flashed.add? [x, y]
    (y - 1 .. y + 1).each do |y|
      next unless @row_range.include? y
      (x - 1 .. x + 1).each do |x|
        next unless @col_range.include? x
        @dumbos[y][x] += 1
        try_flash(flashed, x, y)
      end
    end
  end
end

flashes = STEPS.times.map do |_|
  flashed = Set.new

  @dumbos.map! do |row|
    row.map(&:succ)
  end

  @row_range.each do |y|
    @col_range.each do |x|
      try_flash(flashed, x, y)
    end
  end

  @dumbos.map! do |row|
    row.map {|dumbo| dumbo > 9 ? 0 : dumbo}
  end

  flashed.size
end.sum

p flashes
