#!/usr/bin/ruby
TIMES = 40

lines = ARGF.readlines.map(&:chomp)
template, _ = lines.shift(2)

@insn_map = Hash[lines.map {|line| line.split(' -> ')}]
@tally_cache = Hash.new { |h, k| h[k] = [nil] * TIMES }

def tally(fst, snd, count)
  return {} if count == 0
  key = (fst + snd).freeze
  insn = @insn_map[key]
  return {insn => 1} if count == 1
  @tally_cache[key][count] ||= [
    tally(fst, insn, count - 1),
    {insn => 1},
    tally(insn, snd, count - 1)
  ].reduce do |h1, h2|
    h1.merge(h2) {|k, v1, v2| v1 + v2}
  end#.tap {|h| puts "Cache miss for (#{key}, #{count}) => #{@tally_cache[key][count].inspect}, calculated #{h}"}
end

p template

ttally = template.each_char.each_cons(2).reduce({}) do |h, (fst, snd)|
  h.merge({fst => 1}, tally(fst, snd, TIMES)) { |k, v1, v2| v1 + v2 }
end

ttally[template[-1]] += 1

ttally.values.minmax.then {|min, max| p max - min}
