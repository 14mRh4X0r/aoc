#!/usr/bin/ruby

# Example input: "target area: x=20..30, y=-10..-5"

xrange, yrange = ARGF.gets(chomp: true)[13..].split(', ').map {|e| eval e}

# x[n] = x[n-1] + vx[n-1]
# x[n] = x[0] + n * (vx[0] - (n - 1)) / 2
# vx[n] = vx[n-1] - (vx[n-1] <=> 0)
# n(vx[n] == 0) = vx[0] - 1
# max[x[n]] = x[vx[0]] = vx > 0
#   ? x[0] + vx[0] * (vx[0] + 1) / 2
#   : x[0] - vx[0] * (vx[0] - 1) / 2

# y[n] = y[n-1] + vy[n-1]
# vy[n] = vy[n-1] - 1
# vy[n] = vy[0] - n
# max[y[n]] = vy < 0
#   ? vy
#   : vy[0] * (vy[0] + 1) / 2
#
# y reaches yt => yt = ymax - n * (n + 1) / 2 has positive integer n
# => n**2/2 + n / 2 + (yt - ymax) = 0
# => -1 + sqrt(1 - 4 * (yt - ymax)) / 2 = n
# => ymax > yt

# ---

# yrange always < 0

vy = -yrange.min - 1
ymax = vy * (vy + 1) / 2

p ymax
