#!/usr/bin/ruby
require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'rgl', require: false
end

require 'rgl/adjacency'
require 'rgl/traversal'
require 'rgl/dot'
require 'set'

graph = RGL::AdjacencyGraph[*ARGF.flat_map {|l| l.chomp.split('-')}]
adj = Hash[graph.map do |vertex|
  [vertex, graph.adjacent_vertices(vertex)]
end]

paths = [["start"]]
prev_paths = nil
while paths != prev_paths
  prev_paths = paths
  paths = paths.flat_map do |path|
    next [path] if path[-1] == "end"
    adj[path[-1]].map {|a| path + [a]}
  end
  paths.reject! {|p| p[-1] == "start"}
  paths.reject! do |path|
    set = Set.new
    twice_used = false
    path.reduce(false) do |double, node|
      double || node.match?(/[a-z]+/) && !set.add?(node) && (twice_used || (twice_used = true) && false)
    end
  end
end

paths.reject! {|p| p[-1] != "end"}

p paths.size
