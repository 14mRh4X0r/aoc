#!/usr/bin/ruby
require 'set'

height_map = ARGF.readlines.map {|line| line.chomp.chars.map(&:to_i)}

def get_minima hmap
  hmap.map do |y|
    xmin = []
    xmin << 0 if y[0] < y[1]
    xmin += y.each_cons(3).each_with_index.reduce([]) do |coll, (arr, i)|
      coll << i + 1 if arr[0] > arr[1] && arr[2] > arr[1]
      coll
    end
    xmin << y.length - 1 if y[-1] < y[-2]
    xmin
  end.each_with_index.flat_map do |xmin, y|
    xmin.zip([y] * xmin.length)
  end
end

hmin = get_minima height_map
vmin = get_minima(height_map.transpose).map(&:reverse)
minima = hmin & vmin

minima.map do |p|
  ymax = height_map.size - 1
  xmax = height_map[0].size - 1
  basin = Set.new
  queue = [p]
  until queue.empty?
    x, y = queue.pop
    next if height_map[y][x] == 9
    next unless basin.add? [x, y]
    queue << [x - 1, y] unless x == 0
    queue << [x + 1, y] unless x == xmax
    queue << [x, y - 1] unless y == 0
    queue << [x, y + 1] unless y == ymax
  end
  basin.size
end.max(3).reduce(:*).then {|s| p s}
