store, instr = ARGF.each("\n\n", chomp: true).to_a

store = store.lines(chomp: true)
  .slice(..-2)
  .reverse_each
  .map(&:chars)
  .transpose
  .values_at(*1.step(store.index("\n"), 4))
  .map {|l| l.grep /[A-Z]/}

instr.enum_for(:scan, /^move (\d+) from (\d+) to (\d+)$/)
  .map {|match| match.map(&:to_i)}
  .inject([store, store.map(&:dup)]) do |memo, value|
    s1, s2 = memo
    count, from, to = value
    count.times { s1[to - 1].push s1[from - 1].pop }
    s2[to - 1].push *s2[from - 1].pop(count)
    [s1, s2]
  end
  .map {|s| s.inject("") {|m, a| m << a[-1]}}
  .then(&method(:puts))
