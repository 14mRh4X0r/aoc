$map = map = ARGF.each(chomp: true)
  .map(&:bytes)

queue = map.size.times.to_a.product(map[0].size.times.to_a)

$start = start = queue.find {|c| map.dig(*c) == 'E'.ord}
map[start[0]][start[1]] = 'z'.ord

s = queue.find {|c| map.dig(*c) == 'S'.ord}
map[s[0]][s[1]] = 'a'.ord

$dist = dist = Hash.new(999)
dist[start] = 0
prev = {}

def print_map
  map = $map.map.with_index do |line, y|
    line.map.with_index do |c, x|
      p = [y, x]
      fmt = "\e[0"
      fmt += ";1" if p == $start || $map[y][x] == 'a'.ord
      fmt += sprintf ";38;2;%d;%d;%d",
        (512 - 256 * $dist[p] / 100).clamp(0, 255),
        (256 * $dist[p] / 100).abs.clamp(0, 255),
        (256 * $dist[p] / 100 - 512).clamp(0, 255) if $dist.include? p
      fmt += "m"
      fmt + $map[y][x].chr
    end.join
  end.join "\e[m\n"
  printf "%s\n\c\e[%dA", map, $map.size
end

steps = until queue.empty?
          u = queue.min_by {|u| dist.fetch(u, 999)}
          queue.delete u

          i, j = u
          break dist[u] if map[i][j] == 'a'.ord

          [[i, j-1], [i, j+1], [i-1,j], [i+1,j]]
              .select {|k| queue.include?(k) && map[i][j] - map.dig(*k) <= 1}
              .each do |v|
            alt = dist[u] + 1
            if alt < dist[v]
              dist[v] = alt
              prev[v] = u
            end
          end

          #print_map
        end

#printf "\e[%dB%s\n", map.size, steps
p steps
