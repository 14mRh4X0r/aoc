coords = ARGF.each(chomp: true).map {|x| x.split(" -> ").map {|pt| pt.split(",").map(&:to_i)}}

x_min, x_max = coords.flatten(1).minmax_by(&:first).map(&:first)
y_max = coords.flatten(1).max_by(&:last).last

map = Array.new(y_max + 1) { Array.new(x_max - x_min + 1, true) }

coords.each do |path|
  path.each_cons(2) do |(x1, y1), (x2, y2)|
    if x1 == x2
      Range.new(*[y1, y2].minmax).each {|y| map[y][x1 - x_min] = false}
    elsif y1 == y2
      Range.new(*[x1, x2].minmax).each {|x| map[y1][x - x_min] = false}
    else
      raise "what"
    end
  end
end

puts map.map {|l| l.map {|x| x ? ?. : ?#}.join}.join ?\n

count = (0..).each do |i|
  px, py = 500, 0

  while py < y_max && px.between?(x_min, x_max) && map[py][px - x_min]
    if map[py + 1][px - x_min]
      py += 1
    elsif px - 1 < x_min || map[py + 1][px - 1 - x_min]
      py += 1
      px -= 1
    elsif px + 1 > x_max || map[py + 1][px + 1 - x_min]
      py += 1
      px += 1
    else
      map[py][px - x_min] = false
    end
  end

  break i if py == y_max || !px.between?(x_min, x_max)
end

puts map.map {|l| l.map {|x| x ? ?. : ?#}.join}.join ?\n

p count
