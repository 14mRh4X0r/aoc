GRID = ARGF.each(chomp: true)
  .map(&:chars)
  .map {|xs| xs.map(&:to_i)}

SIZE = GRID.size

def visible?(i, j)
  height = GRID[i][j]
  row = GRID[i]
  col = GRID.transpose[j]
  row[0...j].max < height || row[(j+1)..-1].max < height \
    || col[0...i].max < height || col[(i+1)..-1].max < height
end

coords = (SIZE - 2).times.flat_map do |i|
  (SIZE - 2).times.map {|j| [i + 1, j + 1]}
end

visible = coords.count { |pt| visible?(*pt) }
visible += SIZE * 4 - 4

p visible

def score(i, j)
  height = GRID[i][j]
  row = GRID[i]
  col = GRID.transpose[j]
  check = ->(x) {x >= height}

  left = j - row[0...j].rindex(&check) rescue j
  up = i - col[0...i].rindex(&check) rescue i
  right = 1 + row[(j+1)..-1].index(&check) rescue SIZE - j - 1
  down = 1 + col[(i+1)..-1].index(&check) rescue SIZE - i - 1

  left * up * right * down
end

p coords.map {|pt| score(*pt)}.max
