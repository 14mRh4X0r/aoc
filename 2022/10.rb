$x = 1
$tick = 0
$sum = 0

$out = ""

def draw
  x = $tick % 40
  $out << (x.between?($x - 1, $x + 1) ? '#' : '.')
  $out << "\n" if x == 39
end

def noop
  draw
  $tick += 1
  $sum += $tick * $x if $tick % 40 == 20
end

def addx num
  2.times do
    draw
    $tick += 1
    $sum += $tick * $x if $tick % 40 == 20
  end
  $x += num
end

ARGF.each(chomp: true)
  .map {|line| eval line}

p $sum
puts $out
