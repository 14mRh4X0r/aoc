Array.class_eval do
  include Comparable

  alias cmp <=>

  def <=>(other)
    case other
    when Integer
      self.cmp([other])
    else
      self.cmp(other)
    end
  end
end

Integer.class_eval do
  alias cmp <=>

  def <=>(other)
    case other
    when Array
      [self] <=> other
    when nil
      1
    else
      self.cmp(other)
    end
  end
end

NilClass.class_eval do
  alias cmp <=>

  def <=>(other)
    case other
    when Integer
      -1
    else
      self.cmp(other)
    end
  end
end

pairs = ARGF.each("\n\n", chomp: true).map {|pair| pair.lines(chomp: true).map {|l| eval l}}

pairs.filter_map.with_index do |(first, second), i|
  first < second ? i : nil
end.map(&:succ).then {|is| p is.sum}

DIVIDERS = [[[2]], [[6]]]
sorted = (pairs.flatten(1) + DIVIDERS).sort
DIVIDERS.map {|div| sorted.index(div) + 1}.then {|ds| p ds.reduce(:*)}
