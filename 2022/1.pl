use strict;
use warnings "all";
use List::Util qw(max sum);

my @elves;
my $idx = 0;

while (<>) {
    if (/\d+/) {
        $elves[$idx] += $_;
    } else {
        $idx++;
    }
}

printf "%d\n", max @elves;
printf "%d\n", sum((sort @elves)[-1, -2, -3]);
