SEARCH_AREA = ARGV.shift.to_i + 1

# Scanner => nearest beacon
tbl = Hash[
  ARGF.map do |line|
    /Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)/.match(line) do |match|
      [[match[1].to_i, match[2].to_i],
       [match[3].to_i, match[4].to_i]]
    end
  end
]

# Manhattan distance to nearest beacon
dst = tbl.to_h do |k, v|
  [k, (k[0] - v[0]).abs + (k[1] - v[1]).abs]
end

coords = SEARCH_AREA.times.lazy.flat_map do |x|
  printf "%5.1f%%\r", 100.0 * x / SEARCH_AREA if x % 4000 == 0
  SEARCH_AREA.times.lazy.map {|y| [x, y]}
end

coords.find do |c|
  !tbl.has_value?(c) \
    && dst.each.all? do |(k, v)|
      (k[0] - c[0]).abs + (k[1] - c[1]).abs > v
  end
end.then {|(x, y)| p 4000000 * x + y}
