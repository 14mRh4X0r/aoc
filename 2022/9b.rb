MOVE = {
  R: ->(p) { p[0] += 1 },
  L: ->(p) { p[0] -= 1 },
  U: ->(p) { p[1] += 1 },
  D: ->(p) { p[1] -= 1 }
}

snake = Array.new(10) { [0, 0] }

visited = {[0, 0] => true}

steps = ARGF.each(chomp: true)
  .map(&:split)
  .map {|d,n| [d.to_sym, n.to_i]}

def update(h, t)
  dx, dy = h[0] - t[0], h[1] - t[1]
  if dx.abs > 1 && dy.abs > 1
    t[0] += dx <=> 0
    t[1] += dy <=> 0
  elsif dx.abs > 1
    t[0] += dx <=> 0
    t[1] = h[1]
  elsif dy.abs > 1
    t[1] += dy <=> 0
    t[0] = h[0]
  end
end

steps.each do |d, n|
  n.times do
    MOVE[d].call(snake[0])
    snake.each_cons(2) {|h, t| update(h, t)}
    visited[snake.last.dup] = true
  end
end

p visited.size
