store, instr = ARGF.read.split("\n\n").map {|x| x.lines(chomp: true)}

store = store.reverse_each
    .inject(Array.new(store.pop.split.size) {Array.new}) do |store, line|
  store.size.times do |i|
    store[i].push line[4*i + 1] unless line[4*i + 1] == " "
  end
  store
end

instr.each do |line|
  /move (\d+) from (\d+) to (\d+)/.match line do |m|
    work = m.captures.map(&:to_i)
    work[0].times do
      store[work[2] - 1].push store[work[1] - 1].pop
    end
  end
end

p store.inject("") {|m, s| m << s[-1]}
