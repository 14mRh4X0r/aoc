def priority(c)
  if c.between?('a', 'z')
    1 + c.ord - 'a'.ord
  else
    27 + c.ord - 'A'.ord
  end
end

lines = ARGF.each.to_a

p lines.map {|x| half = x.size / 2; [x[0, half], x[half, half]]}
  .map {|a, b| a.chars.intersection(b.chars)[0]}
  .map {|c| priority(c) }
  .sum

p lines.each_slice(3)
  .map {|slice| slice.map(&:chars)}
  .map {|slice| p slice.pop.intersection(*slice)[0]}
  .map {|c| priority(c) }
  .sum
