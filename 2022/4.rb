ranges = p ARGF.each
  .map {|l| l.chomp.split ?, }
  .map {|rs| rs.map {|r| Range.new(*r.split(?-).map(&:to_i))}}

p ranges.count {|a, b| a.cover?(b) || b.cover?(a)}

p ranges.count {|a, b| !a.to_a.intersection(b.to_a).empty?}
