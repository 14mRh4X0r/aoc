chars = ARGF.read
  .chomp
  .each_char

sop = chars.each_cons(4)
  .find_index { |arr| arr.uniq.size == 4 }

som = chars.each_cons(14)
  .find_index { |arr| arr.uniq.size == 14 }

p sop + 4, som + 14
