require 'set'

$map = map = ARGF.each(chomp: true)
  .map(&:bytes)

size_x = map[0].size
size_y = map.size

$start = start = map.each_with_index {|l, y| x = l.index 'S'.ord; break [y, x] unless x.nil?}
$goal = goal = map.each_with_index {|l, y| x = l.index 'E'.ord; break [y, x] unless x.nil?}

map[start[0]][start[1]] = 'a'.ord
map[goal[0]][goal[1]] = 'z'.ord

$dist = dist = {goal => 0}

def print_map
  map = $map.map.with_index do |line, y|
    line.map.with_index do |c, x|
      p = [y, x]
      fmt = "\e[0"
      fmt += ";1" if [$start, $goal].include? p
      fmt += sprintf ";38;2;%d;%d;%d",
        (512 - 256 * $dist[p] / 100).clamp(0, 255),
        (256 * $dist[p] / 100).abs.clamp(0, 255),
        (256 * $dist[p] / 100 - 512).clamp(0, 255) if $dist.include? p
      fmt += "m"
      fmt + $map[y][x].chr
    end.join
  end.join "\e[m\n"
  printf "%s\e[m\n\c\e[%dA", map, $map.size
end

queue = Queue.new
explored = Set[goal]
queue << goal

tgt = until queue.empty?
  v = queue.pop
  break v if map.dig(*v) == 'a'.ord

  i, j = v
  [[i, j-1], [i, j+1], [i-1,j], [i+1,j]]
      .select {|(k, l)| k.between?(0, size_y - 1) && l.between?(0, size_x - 1)}
      .select {|(k, l)| !explored.include?([k, l]) && map[i][j] - map[k][l] <= 1}
      .each do |w|
    explored << w
    dist[w] = 1 + dist[v]
    queue << w
  end

  #print_map
end

#printf "\e[%dB%d\n", map.size, dist[goal]
p dist[v]
