SCORES = {
  "X" => 1,
  "Y" => 2,
  "Z" => 3
}

MATCH = {
  "A" => %w[Z X Y],
  "B" => %w[X Y Z],
  "C" => %w[Y Z X]
}

split = ARGF.each.map(&:split)

p split.map {|a, b| SCORES[b] + 3 * MATCH[a].index(b)}.sum
p split.map {|a, b| strat = b.ord - 'X'.ord; SCORES[MATCH[a][strat]] + 3 * strat}.sum
