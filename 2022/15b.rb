SEARCH_AREA = ARGV.shift.to_i

# Scanner => nearest beacon
tbl = Hash[
  ARGF.map do |line|
    /Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)/.match(line) do |match|
      [[match[1].to_i, match[2].to_i],
       [match[3].to_i, match[4].to_i]]
    end
  end
]

# Manhattan distance to nearest beacon
dst = tbl.to_h do |k, v|
  [k, (k[0] - v[0]).abs + (k[1] - v[1]).abs]
end

coord = SEARCH_AREA.succ.times do |row|
  printf "%5.1f%%\r", 100.0 * row / SEARCH_AREA if row % 4000 == 0
  left_at_target = Hash[
    dst.filter_map do |k, v|
      [k, v - (k[1] - row).abs] if (k[1] - row).abs <= v
    end
  ]

  ranges = left_at_target.map {|k, v| (k[0] - v..k[0] + v)}
  ranges = ranges.reduce([ranges.shift]) do |memo, range|
    next memo if memo.any? {|r| r.cover? range}

    # extend ranges if possible
    memo.map! do |r|
      if r.member? range.begin
        (r.begin..range.end)
      elsif r.member? range.end
        (range.begin..r.end)
      else
        r
      end
    end

    memo << range

    # Remove overlap
    memo.sort_by!(&:begin)
    memo = memo.reduce([memo.shift]) do |memo, r2|
      r1 = memo.pop
      if r1.cover? r2
        memo << r1
      elsif r2.cover? r1
        memo << r2
      elsif r1.end > r2.begin
        memo << (r1.begin..r2.end)
      else
        memo << r1
        memo << r2
      end
    end
  end

  ## Remove beacons from ranges
  #beacons_at_target = tbl.each_value.filter {|v| v[1] == row}.map(&:first)
  #ranges = ranges.reduce([]) do |memo, range|
  #  beacons_in_range = beacons_at_target.select {|b| range.member? b}.uniq
  #  last = beacons_in_range.sort.reduce(nil) do |prev, b|
  #    memo << (prev || range.begin..b - 1)
  #    b + 1
  #  end
  #  memo << (last || range.begin..range.end)
  #end

  next if ranges.any? {|r| r.cover? (0..SEARCH_AREA)}

  break [0, row] if ranges.first.begin > 0
  break [SEARCH_AREA, row] if ranges.last.end < SEARCH_AREA
  empty = ranges.each do |r|
    break r.end + 1 if r.end >= -1 && r.end < SEARCH_AREA
    nil
  end

  break [empty, row] unless empty.nil?
end

p coord.then {|(x, y)| 4000000 * x + y}
