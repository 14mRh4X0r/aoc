COUNT = 2022

SHAPES = <<EOS.split("\n\n").map do |shape|
####

.#.
###
.#.

..#
..#
###

#
#
#
#

##
##
EOS
  shape.lines(chomp: true).map do |line|
    sprintf("  %- 5s", line).chars.map {|c| c == '#'}
  end.reverse
end

pattern = ARGF.read.chomp.chars
ORIG_PATTERN = pattern.join.freeze

chamber = []

def print_chamber(chamber, shape=nil, y=nil)
  if shape
    (chamber.size - y...shape.size).reverse_each do |y|
      puts shape[y].map {|c| c ? ?@ : ?.}.join
    end
  end
  chamber.each_with_index.reverse_each do |crow, yp|
    crow.map.with_index do |c, i|
      s = shape && (yp - y).between?(0, shape.size - 1) && shape[yp - y][i]
      raise "the fuck" if c && s
      c ? ?# : s ? ?@ : ?.
    end.join.then {|l| puts l}
  end
  puts
end

def print_chamber_slice(chamber, shape, y)
  last = chamber.size < 30 ? chamber.size : 30
  print "\n" * (30 - last)
  print_chamber chamber.last(last), shape, last + y - chamber.size
  $stdin.getc
  print "\e[30A"
end

def collision?(chamber, shape, y)
  chamber ||= []
  shape.zip(chamber.slice(y, shape.size)).reduce(false) do |memo, (srow, crow)|
    crow ||= []
    memo ||= srow.zip(crow).map {|(s,c)| s & c}.any?
  end
end

COUNT.times do |i|
  shape = SHAPES[i % SHAPES.size].dup.map(&:dup)
  chamber += Array.new(3) { Array.new(7, false) }

  shape.each {|s| puts s.map{|c| c ? ?# : ?.}.join}
  puts

  y = chamber.size.pred.downto(-1) do |y|
    print_chamber_slice chamber, shape, y + 1
    case pattern[0]
    when ?<
      at_left = shape.transpose[0].any?
      shape.map(&:rotate!) unless at_left || collision?(chamber, shape.map(&:rotate), y + 1)
    when ?>
      at_right = shape.transpose[-1].any?
      shape.map {|s| s.rotate!(-1)} unless at_right || collision?(chamber, shape.map {|s| s.rotate(-1)}, y + 1)
    end
    pattern.rotate!

    print_chamber_slice chamber, shape, y + 1

    break 0 if y == -1

    # Row empty?
    next if chamber[y].none?

    # Collision?
    break y + 1 if collision?(chamber, shape, y)
  end

  chamber.slice(y, shape.size).each_with_index do |crow, i|
    crow.each_index do |j|
      crow[j] ||= shape[i][j]
    end
  end

  chamber.pop while chamber[-1].none?

end

p chamber.size
