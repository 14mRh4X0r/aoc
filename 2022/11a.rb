class Monkey
  REGEX = Regexp.new <<~'REGEX'.chomp
    Monkey \d+:
      Starting items: (?<items>.*)
      Operation: new = (?<operation>.*)
      Test: divisible by (?<test>\d+)
        If true: throw to monkey (?<if_true>\d+)
        If false: throw to monkey (?<if_false>\d+)
  REGEX

  attr_reader :items, :inspected

  def initialize(items, operation, test, if_true, if_false)
    @@monkeys ||= []
    @items = items
    @operation = operation
    @test = test
    @target = { true => if_true,
                false => if_false }

    @inspected = 0

    @@monkeys << self
  end

  def act
    until @items.empty?
      item = @items.shift
      item = @operation.call(item)
      @inspected += 1
      item /= 3
      target = @target[@test.call(item)]
      @@monkeys[target].items << item
    end
  end

  def self.parse(text)
    data = REGEX.match(text)
    Monkey.new(eval("[#{data[:items]}]"),
               ->(old) { eval data[:operation] },
               ->(item) { item % data[:test].to_i == 0 },
               data[:if_true].to_i, data[:if_false].to_i)
  end
end

pp Monkey::REGEX

ARGF.each("\n\n", chomp: true)
    .map {|monkey| Monkey.parse monkey}
    .then do |monkeys|
  20.times do
    monkeys.each(&:act)
  end

  p monkeys.map(&:inspected).max(2).reduce(:*)
end
