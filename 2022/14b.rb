coords = ARGF.each(chomp: true).map {|x| x.split(" -> ").map {|pt| pt.split(",").map(&:to_i)}}

y_max = coords.flatten(1).max_by(&:last).last + 2
x_min = 500 - y_max + 1

map = Array.new(y_max + 1) { Array.new(2 * y_max - 1, true) }
map[y_max] = [false] * (2 * y_max - 1)

coords.each do |path|
  path.each_cons(2) do |(x1, y1), (x2, y2)|
    if x1 == x2
      Range.new(*[y1, y2].minmax).each {|y| map[y][x1 - x_min] = false}
    elsif y1 == y2
      Range.new(*[x1, x2].minmax).each {|x| map[y1][x - x_min] = false}
    else
      raise "what"
    end
  end
end

def draw_map(map)
  map.map do |line|
    line.each_slice(2).map do |xs|
      next xs[0] ? ?. : ?▌ if xs.size == 1
      case xs
      when [true, true]
        ?.
      when [false, true]
        ?▌
      when [true, false]
        ?▐
      else
        ?█
      end
    end.join
  end.join ?\n
end

puts draw_map map

count = (1..).each do |i|
  px, py = 500, 0

  while py < y_max && map[py][px - x_min]
    if map[py + 1][px - x_min]
      py += 1
    elsif map[py + 1][px - 1 - x_min]
      py += 1
      px -= 1
    elsif map[py + 1][px + 1 - x_min]
      py += 1
      px += 1
    else
      map[py][px - x_min] = false
    end
  end

  break i unless map[0][y_max - 1]
end

puts draw_map map

p count
