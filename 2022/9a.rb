MOVE = {
  R: ->(p) { p[0] += 1 },
  L: ->(p) { p[0] -= 1 },
  U: ->(p) { p[1] += 1 },
  D: ->(p) { p[1] -= 1 }
}

h = [0, 0]
t = [0, 0]

visited = {[0, 0] => true}

steps = ARGF.each(chomp: true)
  .map(&:split)
  .map {|d,n| [d.to_sym, n.to_i]}

steps.each do |d, n|
  n.times do
    MOVE[d].call(h)

    case d
    when :R, :L
      if (h[0] - t[0]).abs > 1
        MOVE[d].call(t)
        t[1] = h[1]
      end
    when :U, :D
      if (h[1] - t[1]).abs > 1
        MOVE[d].call(t)
        t[0] = h[0]
      end
    end
    visited[t.dup] = true
  end
end

p visited.size
