require 'set'
require 'rgl/adjacency'
require 'rgl/dot'

RE = /Valve (?<valve>[A-Z]+) has flow rate=(?<rate>\d+); tunnels? leads? to valves? (?<dest>.*)/

graph = RGL::AdjacencyGraph.new

rates = ARGF.map do |line|
  RE.match(line) do |match|
    match[:dest].split(", ").each do |dest|
      graph.add_edge match[:valve], dest
    end

    [match[:valve], match[:rate].to_i]
  end
end.to_h

dg = graph.to_dot_graph({
  'fontsize' => 14,
  'overlap' => false,
  'vertex' => {
    'label' => ->(vertex) do
      "< #{vertex} <BR/> <FONT POINT-SIZE=\"10\">#{rates[vertex]}</FONT> >"
    end
  }
})

dg.options[:overlap] = false

puts dg
