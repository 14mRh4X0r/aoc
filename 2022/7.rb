FS = {}
SIZES = {}

# Populate directory structure
ARGF.each(chomp: true)
    .inject([]) do |path, line|
  cwd = path.empty? ? FS : FS.dig(*path)
  case line
  when /^\$ cd (.*)/
    case $~[1]
    when '/'
      []
    when '..'
      path[..-2]
    else
      path << $~[1]
    end
  when /^\$/
    path
  when /^dir (.*)/
    cwd[$~[1]] = {}
    path
  when /^(\d+) (.*)/
    cwd[$~[2]] = $~[1].to_i
    path
  else
    raise 'Unknown pattern'
  end
end

def calc_size(path)
  dir = path.empty? ? FS : FS.dig(*path)
  p path, dir
  SIZES[path] ||= dir.reduce(0) do |size, pair|
    name, entry = pair
    case entry
    when Integer
      size + entry
    when Hash
      size + calc_size(path.dup << name)
    else
      raise "#{entry.inspect}"
    end
  end
end

calc_size([])

p SIZES.each_value.select {|x| x < 100_000}.sum

to_free = SIZES[[]] - 40_000_000

p SIZES.each_value.select {|x| x > to_free}.min
